import{ genericRemove, genericList, genericGet, genericSave, genericUpdate } from "../../../shared/db";
import { ErrorMessage } from "../../../shared/types";

const table = process.env.RED_TABLE || '';

export type RedModel = {
    id?: string;
    nombre: string;
};


export function parseBody(body: any): RedModel {
    const { id, nombre } = body;
    const red: RedModel = { id, nombre };
    return red;
}


function validate(red: RedModel): Array<ErrorMessage> {
    let response: Array<ErrorMessage> = [];
    // Se valida a nivel de API Gateway con un schema (se deja de ejemplo por si se quiere hacer validaciones mas complejas)
    // if (!artista.nombre) {
    //     response.push({ field: 'nombre', message: "El nombre es obligatorio" });
    // }
    // if (!artista.apellido) {
    //     response.push({ field: 'apellido', message: "El apellido es obligatorio" });
    // }
    // if (!artista.lat) {
    //     response.push({ field: 'lat', message: "La latitud es obligatorio" });
    // }    
    // if (!artista.lon) {
    //     response.push({ field: 'lon', message: "La longitud es obligatorio" });
    // }    
    return response;
}

export async function list() {
    return genericList(table);
}

export async function get(id) {
    return genericGet(table, id);
}

export async function save(red: RedModel) {
    return genericSave(table, red, validate);
}

export async function update(id: string, red: RedModel) {
    return genericUpdate(table, red, id, validate);
}

export async function remove(id) {
    return genericRemove(table, id);
}
