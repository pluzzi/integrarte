import { NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from "./home.component";
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';


const routes: Routes = [
  {
    path: "",
    data: {
      title: "",
      urls: [{ title: "Home", url: "/home" }, { title: "Home" }],
    },
    component: HomeComponent,
  },
];

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    NgSelectModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class HomeModule { }
