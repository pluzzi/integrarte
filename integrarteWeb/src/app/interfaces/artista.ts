import { Direccion } from './direccion'
import { Idioma } from './idioma'
import { Red } from './red'
import { Tag } from './tag'

export interface Artista {
    id?: string;
    nombre: string;
    apellido: string;
    dni: string;
    fecha_nacimiento: Date;
    telefono: string;
    email: string;
    rol: string;
    biografia: string;
    reel: string;
    galeria: string;
    direccion: Direccion;
    idiomas: Idioma[];
    curriculum: string;
    redes: Red[];
    tags: Tag[];
    imagenes: string[];
    password: string;
    estado: number; // 0: sin validar, 1: validado
    perfil: number; // 0: artista, 1: admin
}
