import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Artista } from '../interfaces/artista';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ArtistaService {

  constructor(
    private httpService: HttpService,
    private http: HttpClient
  ) { }

  getArtistas(criterio: any): Observable<any>{
    return this.http.post<any>(
      this.httpService.getBaseUrl()+'/artistas/search',
      criterio,
      { headers: this.httpService.getHeaders() }
    )
  }

  getArtista(email: string): Observable<any>{
    return this.http.get<any>(
      this.httpService.getBaseUrl()+'/artistas/'+email,
      { headers: this.httpService.getHeaders() }
    )
  }

  addArtista(artista: Artista): Observable<any>{
    return this.http.post<any>(
      this.httpService.getBaseUrl()+'/artistas',
      artista,
      { headers: this.httpService.getHeaders() }
    )
  }

  updateArtista(artista: Artista): Observable<any>{
    return this.http.put<any>(
      this.httpService.getBaseUrl()+'/artistas/' + artista.email,
      artista,
      { headers: this.httpService.getHeaders() }
    )
  }
}
