import { Component, OnInit } from '@angular/core';
import { LocalidadService } from '../services/localidad.service';
import { ProvinciaService } from '../services/provincia.service';
import { TagService } from '../services/tag.service';
import { RedService } from '../services/red.service';
import { IdiomaService } from '../services/idioma.service';
import { Artista } from '../interfaces/artista';
import { AwsService } from '../services/aws.service';
import { ArtistaService } from '../services/artista.service';
import { LocationService } from '../services/location.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';
import { Validators } from '@angular/forms';
import { ImageCroppedEvent } from 'ngx-image-cropper';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  artista: Artista = {
    nombre: "",
    apellido: "",
    dni: "",
    fecha_nacimiento: new Date(),
    telefono: "",
    email: "",
    rol: "",
    biografia: "",
    reel: "",
    galeria: "",
    direccion: {
      calle: "",
      numero: "",
      cp: "",
      provincia_id: "",
      localidad_id: "",
      lat: "",
      lon: ""
    },
    idiomas: [
    ],
    curriculum: "",
    redes: [
    ],
    tags: [
    ],
    imagenes: [],
    password: "",
    estado: 0,
    perfil: 0
  } as Artista;

  red!: any
  url_red!: string

  afuConfig: any
  resetVar!: any

  localidades_list: any
  provincias_list: any
  redes_list: any
  tags_list: any
  idiomas_list: any

  re_password!: string;

  tags!: Array<any>

  uploading: boolean = false;
  uploadingCV: boolean = false;

  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";

  imageChangedEvent: any = '';
  croppedImage: any = '';
  images: any = [];
  latitude: any = '';
  longitude: any = '';

  options: google.maps.MapOptions = {
    center: {lat:-32, lng: -64},
    zoom: 16
  };

  detalle: any = {
    email: "",
    imagen: "",
    title: "",
    info: ""
  };
  constructor(
    private localidadService: LocalidadService,
    private provinciaService: ProvinciaService,
    private redService: RedService,
    private tagService: TagService,
    private idiomaService: IdiomaService,
    private awsService: AwsService,
    private artistaService: ArtistaService,
    private toast: ToastrService,
    private router: Router,
    private firebase: FirebaseService,
    private locationService: LocationService,
  ) {
    this.configUploader()
    this.getProvincias()
    this.getRedes()
    this.getTags()
    this.getIdiomas()
  }

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
    }
    imageLoaded() {
        // show cropper
    }
    cropperReady() {
      // crop ready
    }
    loadImageFailed() {
        // show message
    }

  configUploader(): void{
    this.afuConfig = {
      multiple: false,
      formatsAllowed: ".jpg,.png",
      maxSize: "1",
      uploadAPI:  {
        url:"https://example-file-upload-api",
        method:"POST",
        headers: {
          "Content-Type" : "text/plain;charset=UTF-8",
          "Authorization" : "Bearer ${this.httpService.getToken()}"
        },
        params: {
          'page': '1'
        },
        responseType: 'blob',
      },
      theme: "dragNDrop",
      hideProgressBar: true,
      hideResetBtn: true,
      hideSelectBtn: true,
      fileNameIndex: true,
      replaceTexts: {
        selectFileBtn: 'Select Files',
        resetBtn: 'Reset',
        uploadBtn: 'Upload',
        dragNDropBox: 'Drag N Drop',
        attachPinBtn: 'Attach Files...',
        afterUploadMsg_success: 'Successfully Uploaded !',
        afterUploadMsg_error: 'Upload Failed !',
        sizeLimit: 'Size Limit'
      }
    }
  }

  getLocalidades(): void{
    this.localidadService.getLocalidades(this.artista.direccion.provincia_id).subscribe(result => {
      this.localidades_list = result.items
    })
  }

  getProvincias(): void{
    this.provinciaService.getProvincias().subscribe(result => {
      this.provincias_list = result.items
    })
  }

  getRedes(): void{
    this.redService.getRedes().subscribe(result => {
      this.redes_list = result.items
    })
  }

  getTags(): void{
    this.tagService.getTags().subscribe(result =>{
      this.tags_list = result.items
    })
  }

  getIdiomas(): void{
    this.idiomaService.getIdiomas().subscribe(result =>{
      this.idiomas_list = result.items
    })
  }

  upload(file: any): void{
    console.log(file)
  }

  addRedSocial(){
    this.artista.redes.push({
      id: this.red.id,
      nombre: this.red.nombre,
      url: this.url_red
    })
    this.red = null
    this.url_red = ""
  }

  removeRedSocial(r: any){
    this.artista.redes = this.artista.redes.filter(elt => !(elt.id == r.id && elt.url == r.url));
  }

  getRedById(red: number): string{
    return (this.redes_list as any[]).filter(elt => elt.id == red)[0].nombre;
  }

  changeTags(event: any){
    this.crearTags();
  }

  changeFile(data: any){
    console.log(data.files);
    for (var i = 0; i < data.files.length; i++) {
      this.uploadFile(data.files[i]);
    }
  }

  async changeFileBase64(data: any){
    console.log(data);
    for (var i = 0; i < data.length; i++) {
      this.uploadFilebase64(data[i]);
    }
  }

  addImage(image: any){
    this.images.push(image);
    console.log(this.images);
  }

  uploadFile(file: any): void{
    this.uploading = true;
    this.awsService.uploadFile(file).subscribe(result =>{
      this.artista.imagenes.push(result.url);
      console.log(result)
      this.uploading = false;
    }, error => {
      console.log(error)
      this.uploading = false;
    })
  }

  uploadFilebase64(file: any){
    this.uploading = true;
    this.awsService.uploadFilebase64(file).subscribe(result =>{
      this.artista.imagenes.push(result.url);
      console.log(result)
      this.uploading = false;
      if (this.images.length === this.artista.imagenes.length){
        this.artistaService.addArtista(this.artista).subscribe(
          result => {
            this.firebase.register(this.artista.email, this.artista.password);
            this.firebase.getCurrentUser().subscribe(user => {
              if(user != null || user != undefined) {
                if(!user.emailVerified){
                  user.sendEmailVerification();
                }
                this.firebase.logout();
              }
            });
            this.toast.success('Los datos se han guardado correctamente. Se ha enviado un correo de verificación de E-Mail.');
            this.router.navigate(['/home'])
          },
          error =>{
            this.toast.error('El E-Mail ingresado ya se encuentra registrado.');
          }
        )
      }
    }, error => {
      console.log(error)
      this.uploading = false;
    })
  }

  changeCVFile(data: any){
    for (var i = 0; i < data.files.length; i++) {
      this.uploadCVFile(data.files[i]);
    }
  }

  uploadCVFile(file: any): void{
    this.uploadingCV = true;
    this.awsService.uploadFile(file).subscribe(result =>{
      this.artista.curriculum = result.url;
      this.uploadingCV = false;
    }, error => {
      console.log(error)
      this.uploadingCV = false;
    })
  }

  getLocation() {
    this.locationService.getPosition().then((pos: { lat: any; lng: any; }) => {
        this.latitude = pos.lat;
        this.longitude = pos.lng;
        this.artista.direccion.lat = this.latitude;
        this.artista.direccion.lon = this.longitude;
    });
}

  async registrar(){
    if(this.validate()){
      this.changeFileBase64(this.images);
    }
  }

  crearTags(){
    const tags = new Array<any>();

    this.tags.forEach(async tag =>{
      if (tag.id === undefined){
        const exist = (this.tags_list as any[]).filter(elt => elt.nombre === tag.label)
        if(exist.length === 0){
          await this.tagService.addTag({ nombre: tag.label }).subscribe(result =>{
            this.getTags();
            tags.push(result.item);
          });
        }
      }else{
        tags.push(tag);
      }
    });

    this.artista.tags = tags;
  }



  validate(): boolean {
    var bandera = true

    if (this.artista.nombre==""){
      this.toast.error('Debe ingresar un nombre.');
      bandera = false
    }

    if (this.artista.apellido==""){
      this.toast.error('Debe ingresar un apellido.');
      bandera = false
    }

    if (this.artista.dni==""){
      this.toast.error('Debe ingresar el D.N.I.');
      bandera = false
    }

    if (this.artista.fecha_nacimiento==null){
      this.toast.error('Debe ingresar la fecha de nacimiento.');
      bandera = false
    }

    if (this.artista.telefono==""){
      this.toast.error('Debe ingresar el teléfono.');
      bandera = false
    }

    if (this.artista.email==""){
      this.toast.error('Debe ingresar el E-Mail.');
      bandera = false
    }
    if(!this.artista.email.match("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")){
      this.toast.error('El E-Mail no es válido.');
      bandera = false
    }

    if (this.artista.biografia==""){
      this.toast.error('Debe ingresar la biografía.');
      bandera = false
    }

    if (this.artista.direccion.calle==""){
      this.toast.error('Debe ingresar la calle de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.numero==""){
      this.toast.error('Debe ingresar el número de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.cp==""){
      this.toast.error('Debe ingresar el C.P. de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.provincia_id==""){
      this.toast.error('Debe ingresar la provincia de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.localidad_id==""){
      this.toast.error('Debe ingresar la loalidad de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.lat==""){
      this.toast.error('Debe ingresar la latitud de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.lon==""){
      this.toast.error('Debe ingresar la longitud de la dirección.');
      bandera = false
    }

    if (this.artista.idiomas.length==0){
      this.toast.error('Debe ingresar al menos un idioma.');
      bandera = false
    }

    if (this.artista.redes.length==0){
      this.toast.error('Debe ingresar al menos una red social.');
      bandera = false
    }

    if (this.artista.tags.length==0){
      this.toast.error('Debe ingresar al menos un tag para la búsqueda.');
      bandera = false
    }

    if( this.artista.password != this.re_password){
      this.toast.error('Las passwords ingresadas no coinciden.');
      bandera = false
    }

    return bandera
  }

  deleteFiles(): void{
    this.artista.imagenes = [];
    this.images = [];
    this.croppedImage = '';
  }

  deleteCVFile(): void{
    this.artista.curriculum = "";
  }

  goToHome(){
    this.router.navigate(['/home'])
  }

}

