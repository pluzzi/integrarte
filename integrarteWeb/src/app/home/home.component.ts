import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalidadService } from '../services/localidad.service';
import { ProvinciaService } from '../services/provincia.service';
import { TagService } from '../services/tag.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  tags_list: any
  provincias_list: any
  localidades_list: any
  provincia!: string
  localidad!: string
  tags = new Array();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private tagService: TagService,
    private localidadService: LocalidadService,
    private provinciaService: ProvinciaService

  ) { }

  ngOnInit(): void {
    this.getProvincias()
    this.getTags()
  }

  getLocalidades(): void{
    this.localidadService.getLocalidades(this.provincia).subscribe(result => {
      this.localidades_list = result.items
    })
  }

  getProvincias(): void{
    this.provinciaService.getProvincias().subscribe(result => {
      this.provincias_list = result.items
    })
  }

  getTags(): void{
    this.tagService.getTags().subscribe(result => {
      this.tags_list = result.items
    })
  }

  goToResults(){
    if(this.provincia==undefined) {
      this.provincia = ""
    }
    if(this.localidad==undefined) {
      this.localidad = ""
    }
    this.router.navigate(
      [
        '/results',
        {
          provincia: this.provincia,
          localidad: this.localidad,
          tags: JSON.stringify(this.tags)
        }
      ],
      {
        relativeTo: this.route
      }
    )
  }

  addTag(tag:any): void {
    const t = this.tags.find(t => t === tag.id)
    if(t===undefined){
      this.tags = [...this.tags, tag.id];
    }
  }

}
