import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Artista } from '../interfaces/artista';
import { ArtistaService } from '../services/artista.service';
import { AwsService } from '../services/aws.service';
import { FirebaseService } from '../services/firebase.service';
import { IdiomaService } from '../services/idioma.service';
import { LocalidadService } from '../services/localidad.service';
import { ProvinciaService } from '../services/provincia.service';
import { RedService } from '../services/red.service';
import { TagService } from '../services/tag.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {

  artista: Artista = {
    nombre: "",
    apellido: "",
    dni: "",
    fecha_nacimiento: new Date(),
    telefono: "",
    email: "",
    rol: "",
    biografia: "",
    experiencias: "",
    reel: "",
    galeria: "",
    direccion: {
      calle: "",
      numero: "",
      cp: "",
      provincia_id: "",
      localidad_id: "",
      lat: "",
      lon: ""
    },
    idiomas: [
    ],
    curriculum: "",
    redes: [
    ],
    tags: [
    ],
    imagenes: [],
    password: "",
    estado: 0,
    perfil: 0
  } as Artista;

  red!: any
  url_red!: string

  afuConfig: any
  resetVar!: any

  localidades_list: any
  provincias_list: any
  redes_list: any
  tags_list!: any[]
  idiomas_list!: any[]

  re_password!: string;

  user: any;

  idiomas!: Array<any>;
  tags!: Array<any>;

  uploading: boolean = false;
  uploadingCV: boolean = false;

  imageChangedEvent: any = '';
  croppedImage: any = '';
  images: any = [];
  newImages: any = [];
  carousel: any = [];
  fecha: any;
  constructor(
    private localidadService: LocalidadService,
    private provinciaService: ProvinciaService,
    private redService: RedService,
    private tagService: TagService,
    private idiomaService: IdiomaService,
    private awsService: AwsService,
    private artistaService: ArtistaService,
    private toast: ToastrService,
    private router: Router,
    private firebase: FirebaseService
  ) {

  }

  ngOnInit() {
    this.configUploader()
    this.getProvincias()
    this.getRedes()
    this.getTags()
    this.getIdiomas()
    this.firebase.getCurrentUser().subscribe(result => {
      this.user = result;
      this.getArtista();
    })


  }


  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
  }
  imageLoaded() {
      // show cropper
  }
  cropperReady() {
    // crop ready
  }
  loadImageFailed() {
      // show message
  }

  changeFileBase64(data: any){
    console.log(data);
    let imagesCount = this.images.length + this.newImages.length;
    for (var i = 0; i < data.length; i++) {
      this.uploadFilebase64(data[i], imagesCount);
    }
  }

  addImage(image: any){
    this.newImages.push(image);
    this.carousel = this.carousel.concat(this.newImages);
  }

  uploadFilebase64(file: any, counter: number): void{
    this.uploading = true;
    this.awsService.uploadFilebase64(file).subscribe(result =>{
      this.artista.imagenes.push(result.url);
      this.uploading = false;
      if (counter === this.artista.imagenes.length){
        this.artista.idiomas = new Array();
        this.idiomas.forEach(idioma =>{
          this.artista.idiomas.push(this.idiomas_list.find(elt => elt.id === idioma))
        })
        this.uploading = true;
        this.artistaService.updateArtista(this.artista).subscribe(
          result => {
            this.firebase.updatePassword(this.artista.password);
            this.toast.success('Los datos se han guardado correctamente.');
            this.router.navigate(['/home'])
            this.uploading = false;
          },
          error =>{
            this.toast.error(error);
          }
        )
      }
    }, error => {
      console.log(error)
      this.uploading = false;
    })
  }
  configUploader(): void{
    this.afuConfig = {
      multiple: false,
      formatsAllowed: ".jpg,.png",
      maxSize: "1",
      uploadAPI:  {
        url:"https://example-file-upload-api",
        method:"POST",
        headers: {
          "Content-Type" : "text/plain;charset=UTF-8",
          "Authorization" : "Bearer ${this.httpService.getToken()}"
        },
        params: {
          'page': '1'
        },
        responseType: 'blob',
      },
      theme: "dragNDrop",
      hideProgressBar: true,
      hideResetBtn: true,
      hideSelectBtn: true,
      fileNameIndex: true,
      replaceTexts: {
        selectFileBtn: 'Select Files',
        resetBtn: 'Reset',
        uploadBtn: 'Upload',
        dragNDropBox: 'Drag N Drop',
        attachPinBtn: 'Attach Files...',
        afterUploadMsg_success: 'Successfully Uploaded !',
        afterUploadMsg_error: 'Upload Failed !',
        sizeLimit: 'Size Limit'
      }
    }
  }

  getLocalidades(): void{
    this.localidadService.getLocalidades(this.artista.direccion.provincia_id).subscribe(result => {
      this.localidades_list = result.items
    })
  }

  getProvincias(): void{
    this.provinciaService.getProvincias().subscribe(result => {
      this.provincias_list = result.items
    })
  }

  getRedes(): void{
    this.redService.getRedes().subscribe(result => {
      this.redes_list = result.items
    })
  }

  getTags(): void{
    this.tagService.getTags().subscribe(result =>{
      this.tags_list = result.items
    })
  }

  getIdiomas(): void{
    this.idiomaService.getIdiomas().subscribe(result =>{
      this.idiomas_list = result.items
    })
  }

  getArtista(): void{
    this.artistaService.getArtista(this.user.email).subscribe(result =>{
      this.artista = result.item;
      this.images = this.artista.imagenes;
      this.carousel = this.images;
      this.re_password = this.artista.password;
      this.getIdiomasIdsList();
      this.getTagsIdsList();
      this.getLocalidades();
      this.artista.fecha_nacimiento = new Date(this.artista.fecha_nacimiento);
    },error =>{
      this.toast.error(error);
    })
  }

  getIdiomasIdsList(): void{
    this.idiomas = new Array();

    this.artista.idiomas.forEach(idioma =>{
      this.idiomas.push(idioma.id);
    })
  }

  getTagsIdsList(): void{
    this.tags = new Array();

    this.artista.tags.forEach(tag =>{
      this.tags.push(tag.id);
    })
  }

  upload(file: any): void{
    console.log(file)
  }

  addRedSocial(){
    this.artista.redes.push({
      id: this.red.id,
      nombre: this.red.nombre,
      url: this.url_red
    })
    this.red = null
    this.url_red = ""
  }

  removeRedSocial(r: any){
    this.artista.redes = this.artista.redes.filter(elt => !(elt.id == r.id && elt.url == r.url))
  }

  getRedById(red: number): string{
    return (this.redes_list as any[]).filter(elt => elt.id == red)[0].nombre
  }

  changeTags(event: any){
    this.crearTags()
  }

  crearTags(){
    const tags = new Array<any>();

    this.tags.forEach(async tag =>{
      if (tag.label != undefined){
        const exist = (this.tags_list as any[]).filter(elt => elt.nombre === tag.label)
        if(exist.length === 0){
          await this.tagService.addTag({ nombre: tag.label }).subscribe(result =>{
            this.getTags();
            tags.push(result.item);
          });
        }

      }else{
        const t = this.tags_list.find(t => t.id === tag);
        tags.push(t);
      }
    });

    this.artista.tags = tags;
  }

  changeFile(data: any){
    for (var i = 0; i < data.files.length; i++) {
      this.uploadFile(data.files[i]);
    }
  }

  uploadFile(file: any): void{
    this.uploading = true;
    this.awsService.uploadFile(file).subscribe(result =>{
      if(this.artista.imagenes === undefined ){
        this.artista.imagenes = [];
      }
      this.artista.imagenes.push(result.url)
      console.log(result)
      this.uploading = false;
    }, error => {
      console.log(error)
      this.uploading = false;
    })
  }

  actualizar(): void{
    if(this.validate()){
      if (this.newImages.length > 0) {
        this.changeFileBase64(this.newImages);
      } else {
        this.artista.idiomas = new Array();
        this.idiomas.forEach(idioma =>{
          this.artista.idiomas.push(this.idiomas_list.find(elt => elt.id === idioma))
        })
        this.artistaService.updateArtista(this.artista).subscribe(
          result => {
            this.firebase.updatePassword(this.artista.password);
            this.toast.success('Los datos se han guardado correctamente.');
            this.router.navigate(['/home'])
          },
          error =>{
            this.toast.error(error);
          }
        )
      }
    }
  }

  validate(): boolean {
    var bandera = true

    if (this.artista.nombre==""){
      this.toast.error('Debe ingresar un nombre.');
      bandera = false
    }

    if (this.artista.apellido==""){
      this.toast.error('Debe ingresar un apellido.');
      bandera = false
    }

    if (this.artista.dni==""){
      this.toast.error('Debe ingresar el D.N.I.');
      bandera = false
    }

    if (this.artista.fecha_nacimiento==null){
      this.toast.error('Debe ingresar la fecha de nacimiento.');
      bandera = false
    }

    if (this.artista.telefono==""){
      this.toast.error('Debe ingresar el teléfono.');
      bandera = false
    }

    if (this.artista.email==""){
      this.toast.error('Debe ingresar el E-Mail.');
      bandera = false
    }

    if (this.artista.biografia==""){
      this.toast.error('Debe ingresar la biografía.');
      bandera = false
    }

    if (this.artista.direccion.calle==""){
      this.toast.error('Debe ingresar la calle de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.numero==""){
      this.toast.error('Debe ingresar el número de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.cp==""){
      this.toast.error('Debe ingresar el C.P. de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.provincia_id==""){
      this.toast.error('Debe ingresar la provincia de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.localidad_id==""){
      this.toast.error('Debe ingresar la loalidad de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.lat==""){
      this.toast.error('Debe ingresar la latitud de la dirección.');
      bandera = false
    }

    if (this.artista.direccion.lon==""){
      this.toast.error('Debe ingresar la longitud de la dirección.');
      bandera = false
    }

    if (this.artista.idiomas.length==0){
      this.toast.error('Debe ingresar al menos un idioma.');
      bandera = false
    }

    if (this.artista.redes.length==0){
      this.toast.error('Debe ingresar al menos una red social.');
      bandera = false
    }

    if (this.artista.tags.length==0){
      this.toast.error('Debe ingresar al menos un tag para la búsqueda.');
      bandera = false
    }

    if( this.artista.password != this.re_password){
      this.toast.error('Las passwords ingresadas no coinciden.');
      bandera = false
    }

    return bandera
  }

  deleteFiles(): void{
    this.artista.imagenes = [];
  }

  goToHome():void{
    this.router.navigate(['/home'])
  }

  changeCVFile(data: any){
    for (var i = 0; i < data.files.length; i++) {
      this.uploadCVFile(data.files[i]);
    }
  }

  uploadCVFile(file: any): void{
    this.uploadingCV = true;
    this.awsService.uploadFile(file).subscribe(result =>{
      this.artista.curriculum = result.url;
      this.uploadingCV = false;
    }, error => {
      console.log(error)
      this.uploadingCV = false;
    })
  }

  deleteCVFile(): void{
    this.artista.curriculum = "";
  }

  verificarEmail(): void{
    if(!this.user.emailVerified){
      this.user.sendEmailVerification();
    }
  }
}
