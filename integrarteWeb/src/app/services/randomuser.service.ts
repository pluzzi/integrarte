import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class RandomuserService {

  constructor(
    private http: HttpClient,
    private config: HttpService
  ) { }

  getRandomUser(): Observable<any>{
    return this.http.get<any>(
      'https://randomuser.me/api/?results=1',
      { headers: this.config.getHeaders() }
    )
  }
}
