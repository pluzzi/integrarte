import { AnalysisErrorList } from "aws-sdk/clients/quicksight"

export class ValidationError extends Error {
    errors: Array<ErrorMessage>;
    constructor(messages: Array<ErrorMessage>) {
        super('');
        this.errors = messages;
        this.name = 'ValidationError';
        Object.setPrototypeOf(this, ValidationError.prototype);
    }
    getErrors() : Array<ErrorMessage> {
        return this.errors;
    }
}

export class NotFoundError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'NotFoundError';
        Object.setPrototypeOf(this, NotFoundError.prototype);
    }

}

export type ErrorMessage = {
    field: string;
    message: string;
}

export type Error500Response = {
    status: string;
    message: string;
}

export type ValidationErrorResponse = {
    status: string;
    errors: Array<ErrorMessage>;
}

export type GetResponseParams = {
    item: any;
}

export type ListResponseParams = {
    total: number;
    items: Array<any>;
}

export type GetResponse = {
    status: string;
    item: AnalysisErrorList;
}

export type GetNotFoundResponse = {
    status: string;
}

export type ListResponse = {
    status: string;
    total: number;
    items: Array<any>;
}