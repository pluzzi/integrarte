import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  //private baseUrl = 'http://localhost:3000/dev';
  // private baseUrl = 'https://xji6vnklnf.execute-api.us-east-1.amazonaws.com/uat';
  private baseUrl = environment.server;
  private token = '';
  //curl -v -X OPTIONS https://x7wv29j3h9.execute-api.us-east-1.amazonaws.com/dev
  constructor() {

  }

  public getBaseUrl(): string{
    return this.baseUrl;
  }

  public getToken(): string{
    return this.token;
  }

  public getHeaders(): HttpHeaders {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }
}
