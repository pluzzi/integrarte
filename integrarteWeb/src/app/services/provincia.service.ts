import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ProvinciaService {

  constructor(
    private httpService: HttpService,
    private http: HttpClient
  ) { }

  getProvincias(): Observable<any>{
    return this.http.get<any>(
      this.httpService.getBaseUrl()+'/provincias',
      { headers: this.httpService.getHeaders() }
    )
  }

  getProvincia(id: string): Observable<any>{
    return this.http.get<any>(
      this.httpService.getBaseUrl()+'/provincias/'+id,
      { headers: this.httpService.getHeaders() }
    )
  }
}
