// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  server: 'https://x7wv29j3h9.execute-api.us-east-1.amazonaws.com/dev',
  firebaseConfig: {
    apiKey: "AIzaSyCDon38d7k_cA1_4xnaJ_lmWh9pHw_Jpes",
    authDomain: "xside-auth.firebaseapp.com",
    databaseURL: "https://xside-auth.firebaseio.com",
    projectId: "xside-auth",
    storageBucket: "xside-auth.appspot.com",
    messagingSenderId: "489624436785",
    appId: "1:489624436785:web:9d39771f93bd4da1eb5510"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
