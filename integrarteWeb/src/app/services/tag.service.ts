import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(
    private httpService: HttpService,
    private http: HttpClient
  ) { }

  getTags(): Observable<any>{
    return this.http.get<any>(
      this.httpService.getBaseUrl()+'/tags',
      { headers: this.httpService.getHeaders() }
    )
  }

  addTag(tag: any): Observable<any>{
    return this.http.post<any>(
      this.httpService.getBaseUrl()+'/tags',
      tag,
      { headers: this.httpService.getHeaders() }
    )
  }
}
