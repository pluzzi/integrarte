import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultsComponent } from './results.component';
import { RouterModule, Routes } from '@angular/router';
import { NO_ERRORS_SCHEMA} from '@angular/core';
import { GoogleMapsModule } from '@angular/google-maps';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const routes: Routes = [
  {
    path: "",
    data: {
      title: "",
      urls: [{ title: "Results", url: "/results" }, { title: "Results" }],
    },
    component: ResultsComponent,
  },
];

@NgModule({
  declarations: [ResultsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    GoogleMapsModule,
    NgSelectModule,
    FormsModule,
    NgbModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ResultsModule { }
