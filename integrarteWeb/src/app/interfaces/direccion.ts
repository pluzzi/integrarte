export interface Direccion {
    calle: string;
    numero: string;
    cp: string;
    localidad_id: string;
    provincia_id: string;
    lat: string;
    lon: string;
}
