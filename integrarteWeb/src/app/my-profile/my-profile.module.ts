import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MyProfileComponent } from "./my-profile.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFileUploaderModule } from 'angular-file-uploader';
import { NgSelectModule } from '@ng-select/ng-select';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ComponentsModule } from '../component/component.module';

const maskConfig: Partial<IConfig> = {
  validation: false,
};

const routes: Routes = [
  {
    path: "",
    data: {
      title: "",
      urls: [{ title: "My Profile", url: "/my-profile" }, { title: "My Profile" }],
    },
    component: MyProfileComponent,
  },
];

@NgModule({
  declarations: [MyProfileComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    AngularFileUploaderModule,
    NgSelectModule,
    ShowHidePasswordModule,
    NgxMaskModule.forRoot(maskConfig),
    NgbModule,
    BsDatepickerModule.forRoot(),
    ImageCropperModule,
  ],
})
export class MyProfileModule { }
