import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from "./register.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { NgSelectModule } from '@ng-select/ng-select';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ImageCropperModule } from 'ngx-image-cropper';
import { GoogleMapsModule } from '@angular/google-maps';

const maskConfig: Partial<IConfig> = {
  validation: false,
};

const routes: Routes = [
  {
    path: "",
    data: {
      title: "",
      urls: [{ title: "Register", url: "/register" }, { title: "Register" }],
    },
    component: RegisterComponent,
  },
];

@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    AngularFileUploaderModule,
    NgSelectModule,
    ShowHidePasswordModule,
    NgxMaskModule.forRoot(maskConfig),
    NgbModule,
    BsDatepickerModule.forRoot(),
    ImageCropperModule,
    GoogleMapsModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class RegisterModule { }
