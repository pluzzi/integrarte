import * as model from "./model";
import {
    genericGetOneRequest,
    genericRemoveRequest,
    genericCreateRequest,
    genericListRequest,
    genericUpdateRequest,
} from "../../../shared/helper";

// Faltan los filtros
export async function list(event, context, callback) {
  callback(null, await genericListRequest(event, model.list));
}

export async function get(event, context, callback) {
  callback(null, await genericGetOneRequest(event, event.params.id, model.get));
}

export async function remove(event, context, callback) {
  callback(null, await genericRemoveRequest(event, event.params.id, model.remove));
}

export async function create(event, context, callback) {
  callback(null, await genericCreateRequest(event, model.save, model.parseBody));
}

export async function update(event, context, callback) {
  callback(null, await genericUpdateRequest(event, model.update, model.parseBody));
}
