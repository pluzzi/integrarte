# Integrarte Backend Services


## Desarrollo

### Preparación de la DB

Instalación de DynamoDB local para desarrollo
```npm run db:install```

Importante: cada vez que se inicia la aplicación, localmente, se pierden los datos de la base de datos.

### Correr en desarrollo

```npm run dev```

Al iniciar la aplicación, muestra las rutas de los endpoints configurados (localmente/en desarrollo) los endpoints comienzan con /dev/


### Validaciones

Las validaciones se realizan por medio de Schemas de JSON que corren en el API Gateway. Un ejemplo de validación se puede encontrar en apis/models/artista.json y la inclusión de la misma en los request en el archivo serverless.yml



### Endpoints

#### /provincias

- GET /provincias
- GET /provincias/{id provincia}
- POST /provincias/populate: carga las provincias desde el archivo data.json de la carpeta de provincias

##### Model

```
{
    id: string, 
    nombre: string
}
```

#### /localidades

- GET /provincias/{id provincia}/localidades
- GET /provincias/{id provincia}/localidades/{id municipio}

Se debe incluir la información de localidades con georeferencia?

##### Model

```
{
    id: string, 
    nombre: string,
    provincia_id: string
}
```


#### /artistas

- GET /artistas 
- GET /artistas/{id artista}
- POST /artistas
- PUT /artistas/{id artista}

Para GET /artistas se debe poder definir filtros, ej: GET /artistas?tag=guitarra&lat=123&lon=321 y deberá retornar solo los artistas que cumplen con dicho criterio. 

Existe una herramienta para buscar por georeferencia en los queries de DynamoDB (https://read.acloud.guru/location-based-search-results-with-dynamodb-and-geohash-267727e5d54f) con la cual se puede especificar un "radio" de búsqueda sobre un punto (los que están a x kms de un punto)

##### Model

```
{
    id: string
    nombre: string,
    apellido: string,
    fecha_nacimiento: date,
    biografia: string,
    experiencias: string,
    direccion: {
        calle: string,
        numero: string,
        cp: string,
        localidad: string, // localidad_id?
        provincia: string, // provincia_id?
        lat: number,
        lon: number
    }
    idiomas: [
        {id: string, nombre: string},
        ...
    ],
    curriculum: string, // url al CV
    redes: [
        string,
        string,
        ...
    ],
    tags: [
        {id: string, nombre: string},
        ...
    ]
}
```


#### /tags

- GET /tags

##### Model

```
{
    id: string, 
    nombre: string
}
```

#### /idiomas

- GET /idiomas

##### Model

```
{
    id: string, 
    nombre: string
}
```

## Despliegue

Al hacer un git push se despliega la aplicación actualizando los lambdas existentes. El proceso de despliegue crea las tablas en dynamodb y despliega las funciones lambdas (Serverless). El despliegue se realiza por medio de github actions y la configuración está en el archivo .github/workflows/push.yml

## Arquitectura

La base de datos es DynamoDB, una base de datos documental (similar a mongoDB). Solo se debe definir la clave primaria, los demás campos son definidos a nivel de aplicación. Existe un model por cada tabla (artistas/model, localidades/model, provincias/model).

Todas las peticiones, al ser desplegadas, ingresan por medio de un API Gateway de AWS (similar a un reverse proxy) el cual deriva las peticiones a la función lambda asociada a cada ruta. La validación se realiza por medio de un schema (apis/models/artista.json como ejemplo) lo cual permite que si falla la validación, no se llame a la función lambda (reduciendo el costo de ejecución). Este modelo tiene un costo de uso de API Gateway + Uso de lambda + Uso de DynamoDB - y los costos son por request realizado.

Las funciones lambdas son especificadas en el archivo serverless, así como también las rutas asociadas a cada función.