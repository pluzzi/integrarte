import * as model from "./model";
import {
    genericGetOneRequest,
    genericFilteredListRequest
} from "../../../shared/helper";

function mapDbResponseToHttpResponse(localidad) {
  return {
    id: localidad.id,
    nombre: localidad.nombre,
    provincia_id: localidad.provincia_id
  }
}

export async function list(event, context, callback) {
  callback(null, await genericFilteredListRequest(event, { provincia_id: event.pathParameters.provincia_id }, model.getByProvincia, mapDbResponseToHttpResponse));
}

export async function get(event, context, callback) {
  callback(null, await genericGetOneRequest(event, event.pathParameters.id, model.get));
}