import { RouteInfo } from "./sidebar.metadata";

export const ROUTES: RouteInfo[] = [
  {
    path: "",
    title: "Perfil",
    icon: "mdi mdi-account",
    class: "nav-small-cap",
    extralink: false,
    submenu: [],
  },
  {
    path: "",
    title: "Ingresar",
    icon: "mdi mdi-login",
    class: "",
    extralink: false,
    submenu: [],
  },
  {
    path: "",
    title: "Registrarse",
    icon: "mdi mdi-account-plus",
    class: "nav-small-cap",
    extralink: false,
    submenu: [],
  },
  {
    path: "",
    title: "Salir",
    icon: "mdi mdi-logout",
    class: "",
    extralink: false,
    submenu: [],
  }
];
