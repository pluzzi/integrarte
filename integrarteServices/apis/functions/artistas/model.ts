import{ genericRemove, genericList, genericGet, genericSave, genericUpdate, genericSearch, genericGetByEmail } from "../../../shared/db";
import { ErrorMessage } from "../../../shared/types";
import { IdiomaModel } from '../idiomas/model';
import { TagModel } from '../tags/model';
import { RedModel } from '../redes/model';

const dynamodb = require("serverless-dynamodb-client");
const db = dynamodb.doc;

const table = process.env.ARTISTA_TABLE || '';

export type Direccion = {
    calle: string;
    numero: string;
    cp: string;
    localidad_id: string;
    provincia_id: string;
    lat: string;
    lon: string;
}

export type Criteria = {
    provincia_id: string;
    localidad_id: string;
    tags: TagModel[];
}

export type ArtistaModel = {
    id?: string;
    nombre: string;
    apellido: string;
    dni: string;
    fecha_nacimiento: Date;
    telefono: string;
    email: string;
    rol: string;
    biografia: string;
    reel: string;
    direccion: Direccion;
    idiomas: IdiomaModel[];
    curriculum: string;
    redes: RedModel[];
    tags: string[];
    imagenes: string;
    password: string;
    estado: number;
    perfil: number;
};


export function parseBody(body: any): ArtistaModel {
    const { 
        nombre,
        apellido,
        dni,
        fecha_nacimiento,
        telefono,
        email,
        rol,
        biografia,
        reel,
        direccion,
        idiomas,
        curriculum,
        redes,
        tags,
        imagenes,
        password,
        estado,
        perfil
    } = body;

    const artista: ArtistaModel = { 
        nombre,
        apellido,
        dni,
        fecha_nacimiento,
        telefono,
        email,
        rol,
        biografia,
        reel,
        direccion,
        idiomas,
        curriculum,
        redes,
        tags,
        imagenes,
        password,
        estado,
        perfil
    };
    return artista;
}


async function validate(artista: ArtistaModel) {
    let response: Array<ErrorMessage> = [];
    // Se valida a nivel de API Gateway con un schema (se deja de ejemplo por si se quiere hacer validaciones mas complejas)
    // if (!artista.nombre) {
    //     response.push({ field: 'nombre', message: "El nombre es obligatorio" });
    // }
    // if (!artista.apellido) {
    //     response.push({ field: 'apellido', message: "El apellido es obligatorio" });
    // }
    // if (!artista.lat) {
    //     response.push({ field: 'lat', message: "La latitud es obligatorio" });
    // }    
    // if (!artista.lon) {
    //     response.push({ field: 'lon', message: "La longitud es obligatorio" });
    // }

    const exist = await exists(artista.email);
    if (exist) {
        response.push({ field: 'Email', message: "Existe un usuario registrado con el email ingresado." });
    }

    return response;
}

export async function exists(email) {
    const params = {
        TableName: process.env.ARTISTA_TABLE,
        FilterExpression: 'email = :email',
        ExpressionAttributeValues: {':email': email},
    };
    const data = await db.scan(params).promise();
    return data.Count > 0
  }

export async function list() {
    return genericList(table);
}

export async function search(criteria: Criteria) {
    var expression = ""
    var expressionAttributes = {}

    if(criteria.provincia_id){
        expression+= "direccion.provincia_id = :provincia_id"
        expressionAttributes[":provincia_id"] = criteria.provincia_id
    }

    if(criteria.localidad_id){
        if(expression!=""){
            expression += " AND "
        }
        expression+= "direccion.localidad_id = :localidad_id"
        expressionAttributes[":localidad_id"] = criteria.localidad_id
    }

    if(criteria.tags.length!=0){
        if(expression!=""){
            expression += " AND ( "
        }else{
            expression += " ( "
        }

        for(var i = 0; i < criteria.tags.length; i++){
            const params = {
                TableName: process.env.TAG_TABLE,
                Key: {
                    id: criteria.tags[i],
                },
            };
            const tag = await db.get(params).promise();
            console.log(tag.Item)

            if(i!=0){
                expression += " AND "
            }
            expression+= "contains(tags, :tag"+i.toString()+")"
            expressionAttributes[":tag"+i.toString()] = tag.Item
        }
        expression += " )"
    }
    
    return genericSearch(table, expression, expressionAttributes);
}

export async function get(id) {
    return genericGet(table, id);
}

export async function getByEmail(email) {
    return genericGetByEmail(table, email);
}

export async function save(artista: ArtistaModel) {
    return genericSave(table, artista, validate);
}

export async function update(id: string, artista: ArtistaModel) {
    console.log(id)
    debugger
    return genericUpdate(table, artista, id, validate);
}

export async function remove(id) {
    return genericRemove(table, id);
}
