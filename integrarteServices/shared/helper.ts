import {
    Error500Response,
    GetResponseParams,
    ListResponseParams,
    GetResponse,
    ListResponse,
    GetNotFoundResponse,
    ValidationErrorResponse,
    ErrorMessage,
    ValidationError,
    NotFoundError,
} from "./types";

export function createError500(message) {
    const body: Error500Response = {
        status: "error",
        message: message,
    };
    return { statusCode: 500, body: JSON.stringify(body) };
}

export function createValidationError(messages: Array<ErrorMessage>) {
    const body: ValidationErrorResponse = {
        status: "error",
        errors: messages,
    };
    return { statusCode: 422, body: JSON.stringify(body) };
}

export function createOkResponse(body) {
    return { statusCode: 200, body: JSON.stringify(body) };
}

export function createListOkResponse(params: ListResponseParams) {
    const body: ListResponse = {
        status: "ok",
        items: params.items,
        total: params.total,
    };
    return {
        statusCode: 200,
        body: JSON.stringify(body),
        headers: {
            "Access-Control-Allow-Headers" : "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
        }
    };
}

export function createGetOkResponse(params: GetResponseParams) {
    const body: GetResponse = {
        status: "ok",
        item: params.item,
    };
    return {
        statusCode: 200,
        body: JSON.stringify(body),
        headers: {
            "Access-Control-Allow-Headers" : "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
        }
        
    };
}

export function createGetNotFoundResponse() {
    const body: GetNotFoundResponse = {
        status: "not_found",
    };
    return {
        statusCode: 404,
        body: JSON.stringify(body),
    };
}

export function createDeleteOkResponse() {
    return {
        statusCode: 204,
    };
}

export async function genericListRequest(event: any, modelListFn: any, mapListFn?: any) {
    try {
        const results: any = await modelListFn();
        return createListOkResponse({
            items: mapListFn ? results.Items.map(mapListFn) : results.Items,
            total: results.Count,
        });
    } catch (error) {
        console.log(error);
        return createError500("Error al ejecutar el query");
    }
}

export async function genericSearchRequest(event: any, modelListFn: any, mapListFn?: any) {
    try {
        const criteria = JSON.parse(event.body);
        const results: any = await modelListFn(criteria);
        return createListOkResponse({
            items: mapListFn ? results.Items.map(mapListFn) : results.Items,
            total: results.Count,
        });
    } catch (error) {
        console.log(error);
        return createError500("Error al ejecutar el query");
    }
}

export async function genericFilteredListRequest(event: any, filter: any, modelListFn: any, mapListFn?: any) {
    try {
        const results: any = await modelListFn(filter);
        return createListOkResponse({
            items: mapListFn ? results.Items.map(mapListFn) : results.Items,
            total: results.Count,
        });
    } catch (error) {
        console.log(error);
        return createError500("Error al ejecutar el query");
    }
}

export async function genericGetOneRequest(event: any, pk: string, modelGetFn: any) {
    try {
        const result: any = await modelGetFn(pk);
        if (result) {
            return createGetOkResponse({ item: result.Item ? result.Item : result.Items[0] });
        } else {
            return createGetNotFoundResponse();
        }
    } catch (error) {
        console.log(error);
        return createError500("Error al ejecutar el query");
    }
}

export async function genericRemoveRequest(event: any, pk: string, modelRemoveFn: any) {
    try {
        await modelRemoveFn(pk);
        return createDeleteOkResponse();
    } catch (error) {
        if (error instanceof NotFoundError) {
            return createGetNotFoundResponse();
        } else {
            console.log(error);
            return createError500("Error al ejecutar el query");
        }
    }
}

export async function genericCreateRequest(
    event: any,
    modelCreateFn: any,
    parseBodyFn: any
) {
    const body = JSON.parse(event.body);
    const artista = parseBodyFn(body);
    try {
        const response = await modelCreateFn(artista);
        return createGetOkResponse({ item: response });
    } catch (error) {
        if (error instanceof ValidationError) {
            return createValidationError(error.getErrors());
        } else {
            return createError500("Error al ejecutar el query");
        }
    }
}

export async function genericUpdateRequest(
    event: any,
    modelUpdateFn: any,
    parseBodyFn: any
) {
    const body = JSON.parse(event.body);
    const artista = parseBodyFn(body);
    try {
        console.log(body)
        console.log(artista)
        const response = await modelUpdateFn(body.id, artista);
        return createGetOkResponse({ item: response.Attributes });
    } catch (error: any) {
        if (error instanceof ValidationError) {
            return createValidationError(error.getErrors());
        } else {
            if (error instanceof NotFoundError) {
                return createGetNotFoundResponse();
            } else {
                console.log(error);
                return createError500("Error al ejecutar el query");
            }
        }
    }
}

export async function genericJobRequest(event: any, modelFn: any) {
    try {
        const result: any = await modelFn();
        return createOkResponse({ status: 'ok' });
    } catch (error) {
        console.log(error);
        return createError500("Error al ejecutar la operación");
    }
}