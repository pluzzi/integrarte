import{ genericList, genericGet, genericSave } from "../../../shared/db";
import { ErrorMessage } from "../../../shared/types";

const table = process.env.PROVINCIA_TABLE || '';

export type ProvinciaModel = {
    id?: string;
    nombre: string;
};

function validate(provincia: ProvinciaModel): Array<ErrorMessage> {
    let response: Array<ErrorMessage> = [];
    if (!provincia.nombre) {
        response.push({ field: 'nombre', message: "El nombre es obligatorio" });
    }
    return response;
}

export async function list() {
    const response = {Items: exportedProvincias, Count: exportedProvincias.length};
    return response;
}

export async function get(id) {
  const provincia = exportedProvincias.find(prov => prov.id==id);
  return provincia ? {Item: provincia} : false;
}

const exportedProvincias = [
  { "id": "1",  "nombre": "Buenos Aires" },
  { "id": "2",  "nombre": "Buenos Aires-GBA" },
  { "id": "3",  "nombre": "CABA" },
  { "id": "4",  "nombre": "Catamarca" },
  { "id": "5",  "nombre": "Chaco" },
  { "id": "6",  "nombre": "Chubut" },
  { "id": "7",  "nombre": "Córdoba" },
  { "id": "8",  "nombre": "Corrientes" },
  { "id": "9",  "nombre": "Entre Ríos" },
  { "id": "10", "nombre": "Formosa" },
  { "id": "11", "nombre": "Jujuy" },
  { "id": "12", "nombre": "La Pampa" },
  { "id": "13", "nombre": "La Rioja" },
  { "id": "14", "nombre": "Mendoza" },
  { "id": "15", "nombre": "Misiones" },
  { "id": "16", "nombre": "Neuquén" },
  { "id": "17", "nombre": "Río Negro" },
  { "id": "18", "nombre": "Salta" },
  { "id": "19", "nombre": "San Juan" },
  { "id": "20", "nombre": "San Luis" },
  { "id": "21", "nombre": "Santa Cruz" },
  { "id": "22", "nombre": "Santa Fe" },
  { "id": "23", "nombre": "Santiago del Estero" },
  { "id": "24", "nombre": "Tierra del Fuego" },
  { "id": "25", "nombre": "Tucumán" }
];