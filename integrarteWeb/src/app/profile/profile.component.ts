import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { ArtistaService } from '../services/artista.service';
import { LocalidadService } from '../services/localidad.service';
import { ProvinciaService } from '../services/provincia.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  email!: string;
  artesano: any;
  provincia!: any;
  localidad!: any;

  constructor(
    private route: ActivatedRoute,
    private config: NgbCarouselConfig,
    private artistaSrv: ArtistaService,
    private provinciaService: ProvinciaService,
    private localidadService: LocalidadService
  ) {
    this.config.interval = 10000;
		this.config.wrap = false;
		this.config.keyboard = false;
		this.config.showNavigationArrows = true;
		this.config.showNavigationIndicators = true;
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.email = params.get('email')!;

      this.artistaSrv.getArtista(this.email).subscribe(result => {
        this.artesano = result.item;

        this.provinciaService.getProvincia(this.artesano.direccion.provincia_id).subscribe(result => {
          this.provincia = result.item;
        })

        this.localidadService.getLocalidad(this.artesano.direccion.provincia_id, this.artesano.direccion.localidad_id).subscribe(result => {
          this.localidad = result.item;
        })

      });


    });
  }

  getClass(red: any){
    return "mdi-"+red.nombre;
  }

}
