import { ErrorMessage, ValidationError, NotFoundError } from "./types";
import * as uuid from "uuid";

const dynamodb = require("serverless-dynamodb-client");

const db = dynamodb.doc;

export default db;

export const isEmptyObject = (obj) => {
    for (let i in obj) return false;
    return true;
};

export async function genericList(table: string) {
    const params = {
        TableName: table,
    };
    return db.scan(params).promise();
}

export async function genericSearch(table: string, expression: string, expressionAttributes: any) {
    var params = {}
    
    if(expression!=""){
        params = {
            TableName: table,
            FilterExpression: expression===undefined ? '' : expression,
            ExpressionAttributeValues: expressionAttributes===undefined ? '' : expressionAttributes
        };
    }else{
        params = {
            TableName: table
        };
    }
    
    return db.scan(params).promise();
}

export async function genericGet(table: string, id: string) {
    const params = {
        TableName: table,
        Key: {
            id: id,
        },
    };
    console.log(params);
    const response = await db.get(params).promise();
    return isEmptyObject(response) ? false : response;
}

export async function genericGetByEmail(table: string, email: string) {
    const params = {
        TableName: table,
        FilterExpression: 'email = :email',
        ExpressionAttributeValues: {':email': email}
    };
    const result = db.scan(params).promise();
    return result;
}

export async function genericSave(
    table: string,
    model: any,
    validationFn: any
) {
    const validationResponse: Array<ErrorMessage> = await validationFn(model);
    console.log(validationResponse)
    if (validationResponse.length > 0) {
        throw new ValidationError(validationResponse);
    }
    if (!model.id)
        model.id = uuid.v4();
    const params = {
        TableName: table,
        Item: model,
    };
    await db.put(params, model).promise();
    return model;
}

export async function genericUpdate(
    table: string,
    model: any,
    id: string,
    validationFn: any
) {
    const validationResponse = validationFn(model);
    if (validationResponse.length > 0) {
        throw new ValidationError(validationResponse);
    }
    const exists = await genericGet(table, id);
    if (!exists) {
        throw new NotFoundError(id);
    }
    console.log('Generic update', exists);
    const params = {
        TableName: table,
        Key: {
            id: id,
        },
        ReturnValues: "UPDATED_NEW",
    };
    let updateExpressionArray: Array<string> = [];
    let expressionAttributes = {};
    for (let prop in model) {
        updateExpressionArray.push(`${prop}=:${prop}`);
        expressionAttributes[":" + prop] = model[prop];
    }
    params["ExpressionAttributeValues"] = expressionAttributes;
    params["UpdateExpression"] = "SET " + updateExpressionArray.join(", ");
    return db.update(params).promise();
}

export async function genericRemove(table: string, id: string) {
    const exists = await genericGet(table, id);
    if (!exists) {
        throw new NotFoundError(id);
    }
    const params = {
        TableName: table,
        Key: {
            id: id,
        },
    };
    return db.delete(params).promise();
}
