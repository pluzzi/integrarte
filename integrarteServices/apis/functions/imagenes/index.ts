const AWS = require('aws-sdk');
const parser = require('lambda-multipart-parser');

const S3 = new AWS.S3({
  accessKeyId: 'AKIA6J44OZMJRY4WTG6Q',
  secretAccessKey: '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'
});


const BUCKET_NAME = 'integrarte-be-dev'

export async function upload(event, context, callback) {
  const result = await parser.parse(event);
  const file = result.files[0];
  const fileName = (new Date()).getTime()+ '.' + file.filename.split('.')[1];

  const params = {
    Bucket: BUCKET_NAME,
    Key: fileName,
    Body: file.content,
    ACL: 'public-read',
    ContentType: file.contentType
  };

  await S3.upload(params, function(err, data) {
    console.log(data)
    console.log(err)
  }).promise();

  const url = 'https://integrarte-be-dev.s3.amazonaws.com/'+fileName
  const response = {
    statusCode: 200,
    body: JSON.stringify({
      url: url
    }),
    headers: {
      "Access-Control-Allow-Headers" : "Content-Type",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
    }
  }
  callback(null, response )
  
}



function putS3Object(bucket, key, body){
  return S3.putObject({
    Body: body,
    Bucket: bucket,
    ContentType: "mime/type",
    Key: key
  }).promise();
}

function getSignedUrl(bucket, key){
  const params = { Bucket: bucket, Key: key };
  return S3.getSignedUrl('getObject', params);
}