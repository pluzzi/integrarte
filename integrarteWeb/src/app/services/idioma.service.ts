import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class IdiomaService {

  constructor(
    private httpService: HttpService,
    private http: HttpClient
  ) { }

  getIdiomas(): Observable<any>{
    return this.http.get<any>(
      this.httpService.getBaseUrl()+'/idiomas',
      { headers: this.httpService.getHeaders() }
    )
  }

  addIdioma(idioma: any): Observable<any>{
    return this.http.post<any>(
      this.httpService.getBaseUrl()+'/idiomas',
      idioma,
      { headers: this.httpService.getHeaders() }
    )
  }
}
