import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
import { ImagesService } from './images.service';

@Injectable({
  providedIn: 'root'
})
export class AwsService {
  constructor(
    private httpService: HttpService,
    private http: HttpClient,
    private imagesService: ImagesService,
  ) { }

  uploadFilebase64(file: any): Observable<any> {
    var blob = this.imagesService.dataURItoBlob(file);
    var name = this.imagesService.rename();
    var img = this.imagesService.blobToFile(blob, name);
    const imageForm = new FormData();
    imageForm.append("image", img);
    console.log(imageForm);

    return this.http.put<any>(
      this.httpService.getBaseUrl() + '/imagenes',
      imageForm,
      { headers: this.httpService.getHeaders() }
    )
  }

  uploadFile(file: any): Observable<any> {
    console.log(file);
    const imageForm = new FormData();
    imageForm.append("image", file);
    console.log(imageForm);

    return this.http.put<any>(
      this.httpService.getBaseUrl() + '/imagenes',
      imageForm,
      { headers: this.httpService.getHeaders() }
    )
  }


}
