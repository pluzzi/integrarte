import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';
import { ActivatedRoute, Router } from '@angular/router';
import { RandomuserService } from '../services/randomuser.service';
import { LocalidadService } from '../services/localidad.service';
import { ProvinciaService } from '../services/provincia.service';
import { TagService } from '../services/tag.service';
import { ArtistaService } from '../services/artista.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {
  @ViewChild(MapInfoWindow) infoWindow!: MapInfoWindow;

  tags_list: any
  provincias_list: any
  localidades_list: any
  provincia: any
  localidad: any
  artista_provincia: any
  artista_localidad: any
  tags!: any[]
  artistas_result!: any[]

  center: google.maps.LatLngLiteral = {lat: -32.9455968, lng: -60.7045732}
  zoom = 12
  option: google.maps.MarkerOptions = {draggable: false}

  marcadores!: Array<any>

  detalle: any = {
    email: "",
    imagen: "",
    title: "",
    info: ""
  };

  artesano: any;

  bounds: any;

  @ViewChild(GoogleMap, { static: false }) map!: GoogleMap
  @ViewChild('result') templateRef!: TemplateRef<any>;


  constructor(
    private route: ActivatedRoute,
    private tagService: TagService,
    private localidadService: LocalidadService,
    private provinciaService: ProvinciaService,
    private artistaService: ArtistaService,
    private modalService: NgbModal,
    private http: HttpClient,
    private httpConfig: HttpService
  ) {


    this.route.paramMap.subscribe(params => {
      this.getProvincias()
      this.provincia = params.get('provincia')!
      if(this.provincia==""){
        this.provincia = undefined
      }

      this.getLocalidades()
      this.localidad = params.get('localidad')!
      if(this.localidad==""){
        this.localidad = undefined
      }

      this.getTags()
      const tags = params.get('tags')!
      if(tags!=""){
        this.tags = JSON.parse(params.get('tags')!)
      }

      this.goToResults()
    });
  }

  ngOnInit(): void {
  }

  openInfo(marker: MapMarker, content: string, email: string) {
      const m = this.marcadores.find(m => m.email === email);

      this.detalle.email = m.email;
      this.detalle.imagen = m.imagen
      this.detalle.title = m.title
      this.detalle.info = m.description

      var option: google.maps.InfoWindowOptions = {
        maxWidth: 800
      }

      this.infoWindow.options = option
      this.infoWindow.open(marker)
  }

  /*
  goToDetail(email: string){
    console.log(email)
    this.router.navigate(
      [
        '/profile',
        {
          email: email
        }
      ],
      {
        relativeTo: this.route
      }
    )
  }
  */
  getLocalidades(): void{
    this.localidadService.getLocalidades(this.provincia).subscribe(result => {
      this.localidades_list = result.items
    })
  }

  getProvincias(): void{
    this.provinciaService.getProvincias().subscribe(result => {
      this.provincias_list = result.items
    })
  }

  getTags(): void{
    this.tagService.getTags().subscribe(result => {
      this.tags_list = result.items
    })
  }

  goToResults(){
    const criterio = {
      provincia_id: this.provincia===null ? "" : this.provincia,
      localidad_id: this.localidad===null ? "": this.localidad,
      tags: this.tags===null ? []: this.tags,
    }
    this.artistas_result = []
    this.marcadores = []
    this.artistaService.getArtistas(criterio).subscribe(result => {
      this.artistas_result = result.items;
      //this.artistas_result = this.artistas_result.filter(item => { item.estado === 1 && item.perfil === 0 });
      this.getMarcadores()
    })
  }

  getMarcadores(){
    this.marcadores = new Array()
    this.bounds = new google.maps.LatLngBounds();



    this.artistas_result.forEach(artista =>{
      var description = '<h3>'
      for(var i = 0; i < artista.tags.length; i++) {
        description += '<span class="badge badge-pill badge-primary ml-1">#'+artista.tags[i].nombre+'</span>'
      }
      description += '</h3>'
      this.marcadores.push({
        email: artista.email,
        position: {lat: Number(artista.direccion.lat), lng: Number(artista.direccion.lon ) },
        title: artista.apellido + ', ' + artista.nombre,
        description: description,
        imagen: artista.imagenes != undefined ? artista.imagenes[0] : ""
      })

      this.bounds.extend(new google.maps.LatLng(Number(artista.direccion.lat), Number(artista.direccion.lon )));

    })

    if(this.artistas_result.length === 0){
      this.center = {lat: -40.6075682, lng: -58.4370894}
      this.zoom = 3.5;
      this.modalService.open(this.templateRef, {ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true}).result.then((result) => {

      }, (reason) => {
        const closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });

    }else{
      this.map.fitBounds(this.bounds);
    }


  }

  goToDetail(email: any, profile: any) {
    this.artistaService.getArtista(email).subscribe(result => {
      this.artesano = result.item;

      this.provinciaService.getProvincia(this.artesano.direccion.provincia_id).subscribe(result => {
        this.artista_provincia = result.item;
      })

      this.localidadService.getLocalidad(this.artesano.direccion.provincia_id, this.artesano.direccion.localidad_id).subscribe(result => {
        this.artista_localidad = result.item;
      })

      this.modalService.open(profile, {ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true}).result.then((result) => {

      }, (reason) => {
        const closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });

    });


  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getClass(red: any){
    if(red.nombre === 'Galería de Fotos'){
      return "fa fa-image";
    }
    if(red.nombre === 'Instagram'){
      return "fab fa-instagram";
    }
    if(red.nombre === 'Canal de Youtube'){
      return "fab fa-youtube";
    }
    if(red.nombre === 'Web Personal'){
      return "fa fa-cloud";
    }
    if(red.nombre === 'Facebook'){
      return "fab fa-facebook";
    }
    if(red.nombre === 'Facebook'){
      return "fab fa-facebook";
    }
    if(red.nombre === 'Twitch'){
      return "fab fa-twitch";
    }
    if(red.nombre === 'Reel en Youtube, Vimeo u otros'){
      return "fa fa-play";
    }


  }

}
