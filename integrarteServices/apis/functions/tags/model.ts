import{ genericRemove, genericList, genericGet, genericSave, genericUpdate } from "../../../shared/db";
import { ErrorMessage } from "../../../shared/types";

const table = process.env.TAG_TABLE || '';

export type TagModel = {
    id?: string;
    nombre: string;
};


export function parseBody(body: any): TagModel {
    const { id, nombre } = body;
    const tag: TagModel = { id, nombre };
    return tag;
}


function validate(tag: TagModel): Array<ErrorMessage> {
    let response: Array<ErrorMessage> = [];
    // Se valida a nivel de API Gateway con un schema (se deja de ejemplo por si se quiere hacer validaciones mas complejas)
    // if (!artista.nombre) {
    //     response.push({ field: 'nombre', message: "El nombre es obligatorio" });
    // }
    // if (!artista.apellido) {
    //     response.push({ field: 'apellido', message: "El apellido es obligatorio" });
    // }
    // if (!artista.lat) {
    //     response.push({ field: 'lat', message: "La latitud es obligatorio" });
    // }    
    // if (!artista.lon) {
    //     response.push({ field: 'lon', message: "La longitud es obligatorio" });
    // }    
    return response;
}

export async function list() {
    return genericList(table);
}

export async function get(id) {
    return genericGet(table, id);
}

export async function save(tag: TagModel) {
    return genericSave(table, tag, validate);
}

export async function update(id: string, tag: TagModel) {
    return genericUpdate(table, tag, id, validate);
}

export async function remove(id) {
    return genericRemove(table, id);
}
