import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class RedService {

  constructor(
    private httpService: HttpService,
    private http: HttpClient
  ) { }

  getRedes(): Observable<any>{
    return this.http.get<any>(
      this.httpService.getBaseUrl()+'/redes',
      { headers: this.httpService.getHeaders() }
    )
  }

  addRed(red: any): Observable<any>{
    return this.http.post<any>(
      this.httpService.getBaseUrl()+'/redes',
      red,
      { headers: this.httpService.getHeaders() }
    )
  }
}
