import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class LocalidadService {

  constructor(
    private httpService: HttpService,
    private http: HttpClient
  ) { }

  getLocalidades(provincia_id: string): Observable<any>{
    return this.http.get<any>(
      this.httpService.getBaseUrl() + '/provincias/' + provincia_id + '/localidades',
      { headers: this.httpService.getHeaders() }
    )
  }

  getLocalidad(provincia_id: string, localidad_id: string): Observable<any>{
    return this.http.get<any>(
      this.httpService.getBaseUrl() + '/provincias/' + provincia_id + '/localidades/' + localidad_id,
      { headers: this.httpService.getHeaders() }
    )
  }
}
