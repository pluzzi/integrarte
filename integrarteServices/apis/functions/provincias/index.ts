import * as model from "./model";
import {
    genericGetOneRequest,
    genericListRequest
} from "../../../shared/helper";

function mapDbResponseToHttpResponse(provincia) {
  return {
    id: provincia.id,
    nombre: provincia.nombre,
  }
}

export async function list(event, context, callback) {
    callback(null, await genericListRequest(event, model.list, mapDbResponseToHttpResponse));
}

export async function get(event, context, callback) {
  callback(null, await genericGetOneRequest(event, event.pathParameters.provincia_id, model.get));
}