import { Component, EventEmitter, Output, Input, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ArtistaService } from 'src/app/services/artista.service';
import { FirebaseService } from '../../services/firebase.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.scss'],
})
export class NavigationComponent {
  @Output() toggleSidebar = new EventEmitter<void>();
  @Input() showSearch: boolean = false;
  @Input() showSearchButton: boolean = false;

  isLoged: boolean = false;

  email: string = '';
  password: string = '';
  user: any;

  artista: any;
  imagen: string = "";

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private firebase: FirebaseService,
    private artistaService: ArtistaService,
    private toast: ToastrService
  ) {}

  ngOnInit() {
    this.firebase.getCurrentUser().subscribe(result => {
      this.user = result;
      if(this.user != null || this.user != undefined) {
        this.getArtista();
      }
    })
  }

  getArtista(): void{
    this.artistaService.getArtista(this.user.email).subscribe(result =>{
      this.artista = result.item;
      if(this.artista.imagenes){
        this.imagen = this.artista.imagenes[0];
      }
    });
  }

  login(){
    const result = this.firebase.login(this.email, this.password);
    this.firebase.getCurrentUser().subscribe(result => {
      this.user = result;
      if(this.user != null || this.user != undefined) {
        if(this.user.perfil === 0 ){ // artista
          if(!this.user.emailVerified){
            this.logout()
            this.toast.error('Gracias por registrarte, te enviamos un correo para que actives tu cuenta.');
          }
        }else { // validador

        }

      }
    })

    this.router.navigate(['/home'])
  }

  register(){
    this.router.navigate(['/register'])
  }

  logout(){
    this.firebase.logout();
    this.user = null;
    this.router.navigate(['/home'])
  }

  open(login: any) {
    this.modalService.open(login, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
      this.login()
    }, (reason) => {
      const closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  goToMyProfile(): void{
    this.router.navigate(['/my-profile'])
  }

  goToMyValidador(): void{
    this.router.navigate(['/home'])
  }
}
