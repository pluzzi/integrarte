import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(
    public fireAuth: AngularFireAuth,
    private toast: ToastrService
  ) { }

  async login(email: string, password: string){
    try {
      const result = await this.fireAuth.signInWithEmailAndPassword(email, password);
      return result;
    } catch (error) {
      console.log(error);
      this.toast.error('E-Mail o Password incorrectos.');
    }
  }

  async logout(){
    try {
      await this.fireAuth.signOut();
    } catch (error) {
      console.log(error);
    }
  }

  async register(email: string, password: string){
    try {
      const result = await this.fireAuth.createUserWithEmailAndPassword(email, password);
    return result;
    } catch (error) {
      console.log(error);
    }
  }

  async updatePassword(password: string){
    try {
      this.fireAuth.currentUser.then(user => {
        user?.updatePassword(password)
      })
    } catch (error) {
      console.log(error);
    }
  }

  async updateEmail(email: string){
    try {
      this.fireAuth.currentUser.then(user => {
        user?.updateEmail(email)
      })
    } catch (error) {
      console.log(error);
    }
  }

  getCurrentUser(): Observable<any>{
      return this.fireAuth.authState;
  }
}
